package com.sehochen.video.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class CacheUtils {

    /*
    * 保存数据
    * */
    public static void putString(Context context, String key, String values){

        SharedPreferences sharedPre = context.getSharedPreferences( "sehochen",Context.MODE_PRIVATE);
        sharedPre.edit().putString(key,values).commit();
    }

    /*
     * 获取数据
     * */
    public static String getString(Context context, String key){

        SharedPreferences sharedPre = context.getSharedPreferences( "sehochen",Context.MODE_PRIVATE);
        return sharedPre.getString(key,"");
    }

}
