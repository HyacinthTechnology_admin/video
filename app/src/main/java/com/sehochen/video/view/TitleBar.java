package com.sehochen.video.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.sehochen.video.R;

public class TitleBar extends LinearLayout implements View.OnClickListener {

    private View mtvSeach;
    private View mtvGame;
    private View mTvRecord;

    /*
    * 在代码中实例化该类的时候使用这个方法
    * */
    public TitleBar(Context context) {
        this(context,null);
    }

    /*
    * 当在布局文件使用该类的时候， Android 系统通过这个构造方法实例化该类
    * */
    public TitleBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    /*
    * 当需要设置样式的时候，可以使用该方法
    * */
    public TitleBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /*
    * 当布局文件加载完成的时候回调这个方法
    * */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //得到自己孩子的实例
/*        mtvSeach = getChildAt(2);
        mtvGame = getChildAt(3);*/
        mtvSeach = this.findViewById(R.id.tv_seachs);
        mtvGame = this.findViewById(R.id.tv_game);
        mTvRecord  = this.findViewById(R.id.tv_record);
        //点击事件
        mtvSeach.setOnClickListener(this);
        mtvGame.setOnClickListener(this);
        mTvRecord.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_seachs:
                Log.i("fdfddf","搜索");
                break;
            case R.id.tv_game:
                Log.i("fdfddf","游戏");
                break;
            case R.id.tv_record:
                Log.i("fdfddf","历史");
                break;
        }
    }
}
