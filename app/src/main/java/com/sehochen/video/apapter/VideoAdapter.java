package com.sehochen.video.apapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sehochen.video.R;
import com.sehochen.video.entity.MediaItem;

import java.util.ArrayList;

public class VideoAdapter extends BaseAdapter {
    private final boolean isType;
    private Context context;
    private ArrayList<MediaItem> lists;

    public VideoAdapter(Context context, ArrayList<MediaItem> lists,boolean isType) {
        this.context = context;
        this.lists = lists;
        this.isType = isType;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MediaItem MediaEntity = (MediaItem) getItem(position);
        View view;
        ViewHolder viewHolder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.vedio_item,parent,false);
            viewHolder= new ViewHolder();
            viewHolder.nameTextView = view.findViewById(R.id.tv_video_name);
            viewHolder.videoTime = view.findViewById(R.id.tv_video_time);
            viewHolder.videoSize = view.findViewById(R.id.tv_video_size);
            view.setTag(viewHolder);
        }else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.nameTextView.setText(MediaEntity.getName());
        viewHolder.videoTime.setText("时长："+MediaEntity.getDuration());
        viewHolder.videoSize.setText("大小："+MediaEntity.getSize());

        //如果不是视频，使用自定义播放器
        if(!isType){

        }
        return view;
    }

    class ViewHolder{
        TextView nameTextView;
        TextView videoTime;
        TextView videoSize;
    }
}
