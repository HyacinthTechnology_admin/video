package com.sehochen.video.apapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sehochen.video.R;
import com.sehochen.video.entity.MediaItem;

import org.xutils.x;

import java.util.ArrayList;

public class NetVideoAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<MediaItem> lists;

    public NetVideoAdapter(Context context, ArrayList<MediaItem> lists) {
        this.context = context;
        this.lists = lists;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MediaItem MediaEntity = (MediaItem) getItem(position);
        View view;
        ViewHolder viewHolder;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.netvedio_item,parent,false);
            viewHolder= new ViewHolder();
            viewHolder.videoImg = view.findViewById(R.id.video_img);
            viewHolder.nameTextView = view.findViewById(R.id.tv_video_names);
            viewHolder.videoSize = view.findViewById(R.id.tv_video_sizes);
            view.setTag(viewHolder);
        }else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }
        //Xutils加载图片
//        x.image().bind(viewHolder.videoImg,MediaEntity.getData());

        //Glide加载图片
        Glide.with(context).load(MediaEntity.getData())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.videoImg);

        viewHolder.nameTextView.setText(MediaEntity.getName());
        viewHolder.videoSize.setText("大小："+MediaEntity.getSize());
        return view;
    }

    class ViewHolder{
        TextView nameTextView;
        ImageView videoImg;
        TextView videoSize;
    }
}
