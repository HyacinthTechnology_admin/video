package com.sehochen.video.pager;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sehochen.video.R;
import com.sehochen.video.activity.AudioPlayer;
import com.sehochen.video.activity.SimplePlayer;
import com.sehochen.video.activity.SystemVideoPlay;
import com.sehochen.video.apapter.VideoAdapter;
import com.sehochen.video.base.BasePager;
import com.sehochen.video.entity.MediaItem;

import java.util.ArrayList;

public class AudioPager extends BasePager {

/*    @Override
    protected int getRootResId() {
        return R.layout.fragment_audio;
    }*/
    private ListView mVideoListView;
    private TextView mVideoNot;
    private ProgressBar mBarLoading;
    private Context context;
    private ArrayList<MediaItem> mediaList; //装数据集合


    public AudioPager(Context context) {
        this.context = context;
    }

    @Override
    protected int getRootResId() {
        return R.layout.fragment_video;
    }

    @Override
    protected void initData() {
        super.initData();
        getLocalData();
    }

    /*
     * 必须写在这里面才可以
     * */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mVideoListView = (ListView) getView().findViewById(R.id.video_list_view);
        mVideoNot = (TextView) getView().findViewById(R.id.video_not);
        mBarLoading = (ProgressBar) getView().findViewById(R.id.bar_loading);
        //视频点击事件
        mVideoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaItem mediaItem = mediaList.get(position);
                Toast.makeText(context,"列表："+mediaItem.getName(),Toast.LENGTH_SHORT).show();
                //1.调取系统播放器 隐示意图
/*                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(mediaItem.getData()),"video/*");
                context.startActivity(intent);*/

                //2.调用自己写的播放器 显示意图
                Intent intent = new Intent(context, AudioPlayer.class);
//                intent.setDataAndType(Uri.parse(mediaItem.getData()),"video/*");
                intent.putExtra("position",position);
                startActivity(intent);

            }
        });
    }

    Handler handler = new Handler(){

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(mediaList != null && mediaList.size() > 0 ){
                //有数据
                //设置适配器
                Log.e("ddfsfd",mediaList.toString());
                VideoAdapter videoAdapter = new VideoAdapter(context,mediaList,false);
                mVideoListView.setAdapter(videoAdapter);
                //把文本隐藏
                mVideoNot.setVisibility(View.GONE);
            }else{
                //没数据
                //把文本显示
                mVideoNot.setVisibility(View.VISIBLE);
                Log.e("ddfsfd","mei有数据");
            }
            //ProgressBar隐藏
            mBarLoading.setVisibility(View.GONE);
        }
    };


    /*
     *加载本地视频数据
     * 从本地的sdcard得到数据
     * 1.遍历sdcard,后缀名
     * 2.从内容提供者里面获取视频
     * */
    private void getLocalData() {

        mediaList = new ArrayList<>();
        new Thread(){
            @Override
            public void run() {
                super.run();
                ContentResolver ctxResolver  = context.getContentResolver();

                Uri url = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//                Uri url = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
                String[] objs = {
                        MediaStore.Audio.Media.DISPLAY_NAME, //视频文件的名称
                        MediaStore.Audio.Media.DURATION, //视频文件的时长
                        MediaStore.Audio.Media.SIZE, //视频文件的大小
                        MediaStore.Audio.Media.DATA, //视频文件的绝对地址
                        MediaStore.Audio.Media.ARTIST, //歌曲的演唱者
                };
                Cursor cursor= ctxResolver.query(url,objs,null,null,null);

                if(cursor != null){
                    while (cursor.moveToNext()){
                        MediaItem mediaItem = new MediaItem();
                        String name = cursor.getString(0); //视频名字
                        mediaItem.setName(name);
                        Long duration = cursor.getLong(1); //时长
                        mediaItem.setDuration(duration);
                        Long size = cursor.getLong(2); //大小
                        mediaItem.setSize(size);
                        String data = cursor.getString(3); //视频播放地址
                        mediaItem.setData(data);
                        String artist = cursor.getString(4); //艺术家
                        mediaItem.setArtist(artist);

                        mediaList.add(mediaItem);
                    }
                    cursor.close(); //释放资源
                }

                //发消息
                handler.sendEmptyMessage(10);
            }
        }.start();
    }
}
