package com.sehochen.video.pager;



import com.sehochen.video.R;
import com.sehochen.video.base.BasePager;

public class NetAudioPager extends BasePager {

    @Override
    protected int getRootResId() {
        return R.layout.fragment_net_audio;
    }
}
