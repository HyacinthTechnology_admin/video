package com.sehochen.video.pager;


import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.sehochen.video.R;
import com.sehochen.video.apapter.NetVideoAdapter;
import com.sehochen.video.apapter.VideoAdapter;
import com.sehochen.video.base.BasePager;
import com.sehochen.video.entity.MediaItem;
import com.sehochen.video.utils.CacheUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;

public class NetVideoPager extends BasePager {

    private final Context context;
    private ListView mListView;
    private TextView mTextView;
    private ProgressBar mProgressBar;
    private ArrayList<MediaItem> mLists;

    public NetVideoPager(Context context) {
        this.context = context;
    }
    @Override
    protected int getRootResId() {
        return R.layout.fragment_net_video;
    }

    /*
     * 必须写在这里面才可以
     * */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView = getView().findViewById(R.id.video_list_view);
        mTextView = getView().findViewById(R.id.video_not);
        mProgressBar = getView().findViewById(R.id.bar_loading);

        initDataPlus();

        //检查网络失败,获取缓存数据
        String savaJson = CacheUtils.getString(context,"fengxinzi");
        if(!TextUtils.isEmpty(savaJson)){
            parsingData(savaJson);
        }
    }

    protected void initDataPlus() {
        super.initData();
        RequestParams params = new RequestParams("https://nei.netease.com/api/apimock-v2/40c048de65bcbee635d3168c687df6dc/vedio");
        x.http().get(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.i("联网请求：","onSuccess："+result);
                parsingData(result);
                //缓存数据
                CacheUtils.putString(context,"fengxinzi",result);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                mTextView.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                Log.i("联网请求：","onError："+ex.getMessage());
            }

            @Override
            public void onCancelled(CancelledException cex) {
                Log.i("联网请求：","onCancelled："+cex.getMessage());
            }

            @Override
            public void onFinished() {
                Log.i("联网请求：","onFinished：");
            }
        });
    }


    private void parsingData(String json) {
        //解析数据
        mLists  = parsingJson(json);
        //设置适配器
        if(mLists != null && mLists.size() > 0 ){
            //有数据
            //设置适配器
            Log.i("shuju",mLists.toString());

            NetVideoAdapter netVideoAdapter = new NetVideoAdapter(context,mLists);
            mListView.setAdapter(netVideoAdapter);
            //把文本隐藏
            mTextView.setVisibility(View.GONE);

        }else{
            //没数据
            //把文本显示
            mTextView.setVisibility(View.VISIBLE);
            Log.e("网络视频：","mei有数据");
        }
        mProgressBar.setVisibility(View.GONE);

    }

    /*
    * 手动解析json
    * */
    private ArrayList<MediaItem> parsingJson(String json) {
        ArrayList<MediaItem> mLists = new ArrayList<>();
        try {
            JSONObject jsonObj = new JSONObject(json);

            //optJSONArray("list") 。json中的字段名字
            JSONArray jsonArray = jsonObj.optJSONArray("list");

            if(jsonArray != null && jsonArray.length() > 0){
                for (int i = 0;i < jsonArray.length(); i++){

                    JSONObject jsObjItem = (JSONObject) jsonArray.get(i);

                    if(jsObjItem != null){
                        MediaItem mediaItems = new MediaItem();

                        String name = jsObjItem.optString("name");
                        mediaItems.setName(name);
                        Long size = jsObjItem.optLong("size");
                        mediaItems.setSize(size);
                        String url = jsObjItem.optString("url");
                        mediaItems.setData(url);

                        //将数据添加到集合中
                        mLists.add(mediaItems);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mLists;
    }
}
