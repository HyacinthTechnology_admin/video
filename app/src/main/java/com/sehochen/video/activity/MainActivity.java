package com.sehochen.video.activity;


import android.os.Build;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;


import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.sehochen.video.R;
import com.sehochen.video.base.BasePager;
import com.sehochen.video.pager.AudioPager;
import com.sehochen.video.pager.NetAudioPager;
import com.sehochen.video.pager.NetVideoPager;
import com.sehochen.video.pager.VideoPager;
import com.sehochen.video.utils.CommonUtils;

public class MainActivity extends FragmentActivity {


    private FrameLayout mFrameContent;
    private RadioGroup mButtonMenu;
    private TextView mTvSeach;
    private TextView mTvGame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initAuth();
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23){
            CommonUtils.requestPermissions(this);
        }
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initAuth() {

    }
    private void initView() {

        mFrameContent = (FrameLayout) findViewById(R.id.mian_frame_content);
        mButtonMenu = (RadioGroup) findViewById(R.id.button_menu);

        //设置默认选中的菜单
        mButtonMenu.check(R.id.main_menu_video);
        //设置默认显示首页
        setFragment(new VideoPager(MainActivity.this));

        //设置底部菜单的监听
        mButtonMenu.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    default:
                        //本地视频页面
                        setFragment(new VideoPager(MainActivity.this));
                        break;
                    case R.id.main_menu_audio:
                        //本地音乐页面
                        setFragment(new AudioPager(MainActivity.this));
                        break;
                    case R.id.main_menu_netvideo:
                        //网络视频页面
                        setFragment(new NetVideoPager(MainActivity.this));
                        break;
                    case R.id.main_menu_netaudio:
                        //网络音乐页面
                        setFragment(new NetAudioPager());
                        break;
                }
            }
        });
    }

    //把页面添加到Fragment中
    private void setFragment(BasePager basePager) {
        //1.获取FragmentManager
        FragmentManager manager = getSupportFragmentManager();
        //2.开启事务
        FragmentTransaction ft = manager.beginTransaction();
        //3.替换
        ft.replace(R.id.mian_frame_content, basePager);
        //4.提交
        ft.commit();
    }


}

