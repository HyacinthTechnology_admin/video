package com.sehochen.video.activity;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.Nullable;

import com.sehochen.video.R;

public class SystemVideoPlay extends Activity {
    private VideoView systemPlay;
    private Uri uri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_play);
        systemPlay = findViewById(R.id.system_video_play);
        //播放地址
        uri = getIntent().getData();
        if(uri != null){
            systemPlay.setVideoURI(uri);
        }
        //设置控制面板
        systemPlay.setMediaController(new MediaController(this));

        //准备好的监听
        systemPlay.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            //当底层解码准备好的时候
            @Override
            public void onPrepared(MediaPlayer mp) {
                systemPlay.start(); //开始播放
            }
        });
        //播放出错了的监听
        systemPlay.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(SystemVideoPlay.this,"播放出错：",Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        //播放完成 了的监听
        systemPlay.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(SystemVideoPlay.this,"播放完成：",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
