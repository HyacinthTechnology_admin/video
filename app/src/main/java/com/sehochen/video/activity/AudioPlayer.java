package com.sehochen.video.activity;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.sehochen.video.R;

public class AudioPlayer extends Activity {

    private ImageView mIvPlayIcon;
    private TextView mIvPlayArtist;
    private TextView mIvPlayName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_player);
        initView();
    }

    private void initView() {
        mIvPlayIcon = (ImageView) findViewById(R.id.iv_play_icon);
        mIvPlayArtist = (TextView) findViewById(R.id.iv_play_artist);
        mIvPlayName = (TextView) findViewById(R.id.iv_play_name);

        mIvPlayIcon.setBackgroundResource(R.drawable.music_beat);
        AnimationDrawable animationDra = (AnimationDrawable) mIvPlayIcon.getBackground();
        //直接就开始执行，性能不是最佳的。
        animationDra.start();
    }
}
