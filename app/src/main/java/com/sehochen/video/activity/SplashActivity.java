package com.sehochen.video.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;

import com.sehochen.video.R;

public class SplashActivity extends Activity {
    private Handler handler = new Handler();
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
    }

    private void initView() {
        startMainActivity();
    }

    private boolean startMain = false;
    private void startMainActivity() {
        //定时2秒钟后跳转
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!startMain){
                    startMain = true;
                    //跳转到主页面，并且把当前页面关闭
                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    Log.i(TAG,"當前線程："+Thread.currentThread().getName());
                    finish();
                }
            }
        },2000);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i(TAG,"onTouchEvent："+event.getAction());
        startMainActivity();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        //移除定时器
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
