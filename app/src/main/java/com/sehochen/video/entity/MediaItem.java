package com.sehochen.video.entity;

public class MediaItem {
    private String name; //视频名字
    private Long duration; //时长
    private Long size; //大小
    private String data; //视频播放地址
    private String artist; //艺术家

    public String getName() {
        return name;
    }

    public Long getDuration() {
        return duration;
    }

    public Long getSize() {
        return size;
    }

    public String getData() {
        return data;
    }

    public String getArtist() {
        return artist;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "MediaItem{" +
                "name='" + name + '\'' +
                ", duration=" + duration +
                ", size=" + size +
                ", data='" + data + '\'' +
                ", artist='" + artist + '\'' +
                '}';
    }

}