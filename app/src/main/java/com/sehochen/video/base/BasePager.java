package com.sehochen.video.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.xutils.x;

public abstract class BasePager extends Fragment {
    public View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ininView();
        initData();
        // view在父类中
        x.view().inject(this,view);
        return loadRootView(inflater,container,savedInstanceState);
    }

    /*
    * Fragment 使用findViewById子类必须重写这个方法
    * */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //加载视图（resId布局的id）
    protected View loadRootView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        int resId = getRootResId();
        view = inflater.inflate(resId,container,false);
        return view;
    }
//    protected abstract View loadRootView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState);

    //子类必须实现，返回布局的id
    protected abstract int getRootResId();

    //初始化数据
    protected  void initData(){

    }

    //初始化view
    protected  void ininView(){

    }
}
